<div class="col-md-3">
    <!--Begin-->
    <div class="box-sale-policy ng-scope" ng-controller="moduleController" ng-init="initSalePolicyController('Shop')">
        <h3><span>Lĩnh vực kinh doanh</span></h3>
        <div class="sale-policy-block">
            <ul>
                <li>Cung cấp và lắp đặt</li>
                <li>Bảo trì và sửa chữa</li>
                <li>Trung và đại tu thang máy</li>
                <li>Tư vấn và thiết kế</li>
            </ul>
        </div>  
    </div>
        <div class="box-sale-policy ng-scope" ng-controller="moduleController" ng-init="initSalePolicyController('Shop')">
        <h3><span>Sản phẩm kinh doanh</span></h3>
        <div class="sale-policy-block">
            <ul>
                <li>Thang tải khách</li>
                <li>Thang tải hàng</li>
                <li>Thang quan sát</li>
                <li>Thang tải bệnh</li>
                <li>Thang tải thức ăn</li>
                <li>Thang cuốn</li>
            </ul>
        </div>  
    </div>
<div class="box-product">
    <h3>
        <span>
            Sản phẩm MỚI
        </span>
    </h3>
        <div class="box-product-block">
        @if(!empty($topproductnew))
            @foreach($topproductnew as $i => $newproduct)
                <div class="item">
                <div class="image image-resize">
                    <a href="/san-pham/chi-tiet/{{$newproduct->id}}" title="{{$newproduct->title}}">
                        <img src="{{$newproduct->thumbnail}}" class="img-responsive">
                    </a>
                </div>
                <div class="right-block">
                    <h2 class="name">
                        <a href="/san-pham/chi-tiet/{{$newproduct->id}}" title="{{$newproduct->title}}">{{$newproduct->title}}</a>
                    </h2>                    
                </div>
                <div class="shadow">
                </div>
            </div> 
            @endforeach
        @endif  
        </div>
    </div>
</div>

</div>
<div style="border-bottom: 2px solid #70ba28;width: 100%;padding-top: 5px"></div>
</div>
