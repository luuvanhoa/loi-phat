@extends('layouts.front')
@section('content')
    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-9">
                    <?php echo $article->content;?>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content_js')
@endsection